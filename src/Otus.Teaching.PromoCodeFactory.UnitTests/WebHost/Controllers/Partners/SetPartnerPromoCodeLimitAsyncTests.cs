﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Kernel;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize((new AutoMoqCustomization()));
            fixture.Customizations.Add(new TypeRelay(typeof(IPartnersService), typeof(PartnersService)));
            
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        //TODO: Add Unit Tests
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit().NoActive();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RenewLimitWithActiveLimit_ZeroizeNumberIssuedPromoCodes()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RenewLimitWithActiveLimit_CancelPreviousLimit()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var limit = partner.PartnerLimits.First();

            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            limit.CancelDate.Should().BeBefore(DateTime.Now);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_LimitExpired_NotChangeNumberIssuedPromoCodes()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit().WithExpiredLimit();
            var number = partner.NumberIssuedPromoCodes;

            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            partner.NumberIssuedPromoCodes.Should().Be(number);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_EmptyNewLimit_ReturnBadRequest()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest().Empty();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_AddNewLimit_ReturnCreatedAtActionResult()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            var actionResult = result.Should().BeAssignableTo<CreatedAtActionResult>();
            actionResult.Which.RouteValues["id"].Should().Be(partner.Id, "не совпадает Id партнера");
            actionResult.Which.RouteValues["limitId"]
                .Should().BeAssignableTo<Guid>("не совпадает Id лимита").Which
                .Should().NotBe(new Guid(), "создан некорректный Guid у Id лимита");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_AddNewLimit_SaveNewLimit()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();
            
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            partner.PartnerLimits.Should()
                .Contain(x => x.Limit == request.Limit && x.EndDate == request.EndDate, 
                    "Не совпадает добавленный лимит и/или дата");
        }
    }
}