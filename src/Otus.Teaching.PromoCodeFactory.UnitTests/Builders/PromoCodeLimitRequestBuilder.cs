﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest GetNewLimitRequest() => new SetPartnerPromoCodeLimitRequest()
        {
            EndDate = new DateTime(2021, 11, 9),
            Limit = 1_000
        };

        public static SetPartnerPromoCodeLimitRequest Empty(this SetPartnerPromoCodeLimitRequest limitRequest)
        {
            limitRequest.Limit = 0;

            return limitRequest;
        }
    }
}