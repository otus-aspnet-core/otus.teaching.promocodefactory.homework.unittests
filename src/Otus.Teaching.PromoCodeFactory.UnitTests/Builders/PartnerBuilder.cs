﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Partner GetNewPartnerWithOneLimit() => new Partner()
        {
            Id = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"),
            Name = "Sony",
            NumberIssuedPromoCodes = 378,
            IsActive = true,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2021, 07, 9),
                    EndDate = new DateTime(2021, 10, 9),
                    Limit = 100
                }
            }
        };

        public static Partner NoActive(this Partner partner)
        {
            partner.IsActive = false;

            return partner;
        }

        public static Partner WithExpiredLimit(this Partner partner)
        {
            var limit = partner.PartnerLimits.First();
            limit.CancelDate = DateTime.Now;

            return partner;
        }
    }
}