﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application.Services.Partners
{
    public class CancelPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IPartnersService _partnersService;

        public CancelPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize((new AutoMoqCustomization()));
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = fixture.Build<IPartnersService>().Create<PartnersService>();
        }
        
        [Fact]
        public async void CancelPromoCodeLimitAsyncTests_ActiveLimit_CancelActiveLimit()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var limit = partner.PartnerLimits.First();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.CancelPromoCodeLimitAsync(partner);

            limit.CancelDate.Should().BeBefore(DateTime.Now);
        }
        
        [Fact]
        public async void CancelPromoCodeLimitAsyncTests_LimitExpired_ReturnNull()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit().WithExpiredLimit();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.CancelPromoCodeLimitAsync(partner);

            result.Should().BeNull();
        }
    }
}