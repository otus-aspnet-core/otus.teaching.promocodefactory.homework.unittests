﻿using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application.Services.Partners
{
    public class GetActivePartnerAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IPartnersService _partnersService;

        public GetActivePartnerAsyncTests()
        {
            var fixture = new Fixture().Customize((new AutoMoqCustomization()));
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = fixture.Build<IPartnersService>().Create<PartnersService>();
        }

        [Fact]
        public async void GetActivePartnerAsync_PartnerIsNotFound_ThrowNotFoundException()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = () => _partnersService.GetActivePartnerAsync(partnerId);

            // Assert
            await act.Should().ThrowAsync<NotFoundException>();
        }

        [Fact]
        public async void GetActivePartnerAsync_PartnerIsNotActive_ThrowBadRequestException()
        {
            // Arrange
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit().NoActive();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            Func<Task> act = () => _partnersService.GetActivePartnerAsync(partner.Id);

            // Assert
            await act.Should().ThrowAsync<BadRequestException>();
        }
    }
}