﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application.Services.Partners
{
    public class AddPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IPartnersService _partnersService;

        public AddPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize((new AutoMoqCustomization()));
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = fixture.Build<IPartnersService>().Create<PartnersService>();
        }
        
        [Fact]
        public async void AddPromoCodeLimitAsyncTests_EmptyNewLimit_ReturnBadRequestException()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest().Empty();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            Func<Task> act = () => _partnersService.AddPromoCodeLimitAsync(partner.Id, request.Limit, request.EndDate);

            await act.Should().ThrowAsync<BadRequestException>();
        }
        
        [Fact]
        public async void AddPromoCodeLimitAsyncTests_LimitExpired_NotChangeNumberIssuedPromoCodes()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit().WithExpiredLimit();
            var number = partner.NumberIssuedPromoCodes;

            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.AddPromoCodeLimitAsync(partner.Id, request.Limit, request.EndDate);

            partner.NumberIssuedPromoCodes.Should().Be(number);
        }
        
        [Fact]
        public async void AddPromoCodeLimitAsyncTests_ActiveLimit_CancelActiveLimit()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var limit = partner.PartnerLimits.First();
            
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.AddPromoCodeLimitAsync(partner.Id, request.Limit, request.EndDate);

            limit.CancelDate.Should().BeBefore(DateTime.Now);
        }
        
        [Fact]
        public async void AddPromoCodeLimitAsyncTests_RenewLimitWithActiveLimit_ZeroizeNumberIssuedPromoCodes()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.AddPromoCodeLimitAsync(partner.Id, request.Limit, request.EndDate);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async void AddPromoCodeLimitAsyncTests_AddNewLimit_ReturnNewLimitId()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.AddPromoCodeLimitAsync(partner.Id, request.Limit, request.EndDate);

            result.Should().NotBe(new Guid(), "создан некорректный Guid у Id лимита");
        }
        
        [Fact]
        public async void AddPromoCodeLimitAsyncTests_AddNewLimit_SaveNewLimit()
        {
            var partner = PartnerBuilder.GetNewPartnerWithOneLimit();
            var request = PromoCodeLimitRequestBuilder.GetNewLimitRequest();
            
            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var result = await _partnersService.AddPromoCodeLimitAsync(partner.Id, request.Limit, request.EndDate);

            partner.PartnerLimits.Should()
                .Contain(x => x.Limit == request.Limit && x.EndDate == request.EndDate, 
                    "Не совпадает добавленный лимит и/или дата");
        }
    }
}