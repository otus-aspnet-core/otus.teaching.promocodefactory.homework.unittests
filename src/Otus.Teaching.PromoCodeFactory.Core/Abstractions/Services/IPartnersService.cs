﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPartnersService
    {
        Task<Partner> GetActivePartnerAsync(Guid id);

        Task<Guid> AddPromoCodeLimitAsync(Guid partnerId, int limit, DateTime endDate);

        Task<Guid?> CancelPromoCodeLimitAsync(Guid partnerId);

        Task<Guid?> CancelPromoCodeLimitAsync(Partner partner);
    }
}