﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Application.Services
{
    public class PartnersService
        : IPartnersService
    {
        private readonly IRepository<Partner> _partnerRepository;

        public PartnersService(IRepository<Partner> partnerRepository)
        {
            _partnerRepository = partnerRepository;
        }

        public async Task<Partner> GetActivePartnerAsync(Guid partnerId)
        {
            var partner = await _partnerRepository.GetByIdAsync(partnerId);
            if (partner == null)
            {
                throw new NotFoundException("Не найден партнер");
            }

            if (!partner.IsActive)
            {
                throw new BadRequestException("Данный партнер неактивен");
            }

            return partner;
        }

        public async Task<Guid> AddPromoCodeLimitAsync(Guid partnerId, int limit, DateTime endDate)
        {
            var partner = await GetActivePartnerAsync(partnerId);

            if (limit <= 0)
            {
                throw new BadRequestException("Лимит должен быть больше 0");
            }

            //При установке лимита нужно отключить предыдущий лимит
            if ((await CancelPromoCodeLimitAsync(partner)).HasValue)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;
            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.NewGuid(),
                Limit = limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = endDate
            };

            partner.PartnerLimits.Add(newLimit);

            await _partnerRepository.UpdateAsync(partner);

            return newLimit.Id;
        }

        public async Task<Guid?> CancelPromoCodeLimitAsync(Guid partnerId)
        {
            var partner = await GetActivePartnerAsync(partnerId);

            return await CancelPromoCodeLimitAsync(partner);
        }

        public async Task<Guid?> CancelPromoCodeLimitAsync(Partner partner)
        {
            var activeLimit = partner.PartnerLimits
                .FirstOrDefault(x => !x.CancelDate.HasValue);

            if (activeLimit == null)
            {
                return null;
            }

            activeLimit.CancelDate = DateTime.Now;

            await _partnerRepository.UpdateAsync(partner);

            return activeLimit.Id;
        }
    }
}